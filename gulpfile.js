const gulp = require('gulp');
const browserSync = require('browser-sync').create();

gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
});

gulp.task('watch', function() {
    gulp.watch('./*.html', browserSync.reload);
    gulp.watch('./*.css', browserSync.reload);
    gulp.watch('./*.js', browserSync.reload);
});

gulp.task('default', gulp.series('browser-sync', 'watch'));
